<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

	public function index()
	{
        $this->load->view('index');
	}

	public function company()
	{
        $this->load->view('company');
	}

	public function contactus()
	{
        $this->load->view('contactus');
	}

	public function homepage()
	{
        $this->load->view('homepage');
	}

	public function landingpage()
	{
        $this->load->view('landingpage');
	}

	public function mobile()
	{
        $this->load->view('mobile');
	}

	public function portfolio()
	{
        $this->load->view('portfolio');
	}

	public function shoppingmall()
	{
        $this->load->view('shoppingmall');
	}

	public function tech()
	{
        $this->load->view('tech');
	}

  public function sendmail() {
      $json = $this->email();
      $this->json($json);
  }

  public function uploadfiles() {
      $json = $this->upload('file');
      return $this->json($json);
  }

}
