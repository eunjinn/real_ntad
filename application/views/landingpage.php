<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Newturn Tree AD">
    <meta name="description" content="The most effective way to reach customers, Rich Media">

    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="icon" type="image/x-icon">

    <!-- SNS -->
    <meta property="og:title" content="Newturn Tree AD" />
    <meta property="og:site_name" content="Newturn Tree AD"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://newturntreead.com/" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="Newturn Tree AD" />

    <title>Newturn Tree AD WEB</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?=base_url('assets/lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/lib/animate/animate.css" rel="stylesheet')?>" type="text/css">

    <link href="<?=base_url('assets/css/global.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/sub.css')?>" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="<?=base_url('assets/images/logo.png')?>" />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/homepage">홈페이지</a>
                        </li>
                        <li>
                            <a href="/sub/shoppingmall">쇼핑몰</a>
                        </li>
                        <li>
                            <a href="/sub/mobile">모바일/앱</a>
                        </li>
                        <li>
                            <a href="/sub/landingpage">랜딩페이지</a>
                        </li>
                        <li>
                            <a href="/sub/tech">기술력</a>
                        </li>
                        <li>
                            <a href="/sub/portfolio">포트폴리오</a>
                        </li>
                        <li>
                            <a href="/sub/company">회사소개</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">견적문의</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Banner start -->
    <section id="sub-bg">
        <div class="contaier-fluid">
            <div class="subpage-banner">
                <div class="banner-cover">
                    <div class="container">
                        <div class="row wow fadeInLeft">
                            <p>확실한 광고효과! 이탈율 최소화! 시선을 강탈하는 광고!</p>
                            <p>Landingpage</p>
                            <p>데이터 분석을 통한 성공적인 광고 단시간에 핵심 정보를 제공하여 이탈율을 최소화 합니다.<br>Newturn Tree AD는 풍부한 경험과 뛰어난 기술력을 바탕으로 작업을 합니다.</p>
                        </div>
                    </div>
                </div>
                <div class="banner-background home"></div>
                <div class="container banner-img">
                    <div class="landing">
                        <img class="wow fadeInDown" src="<?=base_url('assets/images/landingpage/landing.png')?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner_END -->

    <!-- Advantage -->
    <section id="advantage" class="sub-position wow fadeInUp">
        <div class="container">
            <div class="section-header mt120">
                <b class="point">Why?</b>
                <h3>랜딩페이지는 왜 뉴턴트리AD에 맡겨야 할까?</h3>
            </div>
            <div class="row">
                <div class="advan_img">
                    <img src="<?=base_url('assets/images/landingpage/landing2.png')?>" alt="">
                </div>
            </div>
            <div class="row advantage">
                <div class="col-md-3">
                    <img src="<?=base_url('assets/images/advantage/1.png')?>" alt="">
                    <p class="title">성공적인 비즈니스 제시</p>
                    <p>
                        데이터 기반의 브랜드 진단을 통해 가장 효과적인 최적의 비즈니스를 제안해드립니다.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="<?=base_url('assets/images/advantage/2.png')?>" alt="">
                    <p class="title">풍부한 노하우와 기술력</p>
                    <p>
                        고급인력과 전문기술력을 바탕으로 기본에 가장 충실한 좋은 홈페이지를 만들 수 있습니다.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="<?=base_url('assets/images/advantage/3.png')?>" alt="">
                    <p class="title">신속하고 체계적인 고객응대</p>
                    <p>
                        고객지원부서의 신속한 응대를 통해 상황발생시 언제든 신속한 조치가 가능합니다.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="<?=base_url('assets/images/advantage/4.png')?>" alt="">
                    <p class="title">장기적인 사후관리 서비스</p>
                    <p>
                        신뢰를 기반으로한 지속적인 사후관리를 통해 장기적인 서비스 안정성을 보장합니다.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- Advantage_END -->

    <!-- Step -->
    <section id="step" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>랜딩페이지 제작순서</h3>
                <p>아래와 같은 순서대로 홈페이지 제작이 이루어집니다.</p>
            </div>
            <div class="row step">
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/1.png')?>" alt="">
                    <p>Step.1</p>
                    <p>제작의뢰</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/2.png')?>" alt="">
                    <p>Step.2</p>
                    <p>견적산출</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/3.png')?>" alt="">
                    <p>Step.3</p>
                    <p>계약체결</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/4.png')?>" alt="">
                    <p>Step.4</p>
                    <p>자료수집 및 일정수립</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/5.png')?>" alt="">
                    <p>Step.5</p>
                    <p>사이트 기획</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/6.png')?>" alt="">
                    <p>Step.6</p>
                    <p>사이트 디자인</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/7.png')?>" alt="">
                    <p>Step.7</p>
                    <p>디자인시안 확인</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/8.png')?>" alt="">
                    <p>Step.8</p>
                    <p>프로그래밍</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/9.png')?>" alt="">
                    <p>Step.9</p>
                    <p>검수 및 테스트</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/10.png')?>" alt="">
                    <p>Step.10</p>
                    <p>고객검수</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/11.png')?>" alt="">
                    <p>Step.11</p>
                    <p>AS 및 보수</p>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <img src="<?=base_url('assets/images/step/12.png')?>" alt="">
                    <p>Step.12</p>
                    <p>사이트 오픈</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Step_END -->

    <!-- Ready to -->
    <section id="readyto" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>사전 준비사항</h3>
                <p>랜딩페이지 제작에 필요한 자료를 사전에 준비해주시면 더욱 빠른 작업진행이 가능합니다.</p>
            </div>
            <div class="row readyto">
                <div class="col-md-4">
                    <img src="<?=base_url('assets/images/ready/1.jpg')?>" alt="">
                    <p class="title">아이디어 구상</p>
                    <p>
                        제작하고 싶은 랜딩페이지의 형태, 컨셉, 분위기, 레이아웃, 등을 참고할 사이트를 말씀해주세요.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="<?=base_url('assets/images/ready/2.jpg')?>" alt="">
                    <p class="title">기본자료 준비</p>
                    <p>
                        기본자료(회사소개서, 사업자등록증, 인사말, 약도, 관련사진, 카달로그, 책자, 등)를 준비해 주세요.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="<?=base_url('assets/images/ready/3.jpg')?>" alt="">
                    <p class="title">사이트맵 구상</p>
                    <p>
                        사이트 메뉴를 대략적으로 구상해주시면 최적의 사이트맵을 제안해드립니다.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- Ready to_END -->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#top"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <? require_once(APPPATH."views/footer.php");?>
    <!-- Javascript files -->
    <script src="<?=base_url('assets/lib/sticky/jquery.sticky.js')?>"></script>
    <script src="<?=base_url('assets/lib/wow/wow.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/isotope/isotope.pkgd.min.js')?>"></script>

    <script src="<?=base_url('assets/js/port.js')?>"></script>
</body>
</html>
