
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 p0">
					<div class="foot_logo">
						<img src="<?=base_url('assets/images/logo.png')?>" title="로고" alt="로고">
					</div>
					<div class="foot_cont">
						상호명 : (주)뉴턴트리애드(NTAD) | 대표자 : 황준범 | 주소 : 서울특별시 강남구 언주로167길 35, 지하1층<br>
						대표전화 : 02-544-1117 | 이메일 : info@newturntreead.com<br>
						사업자등록번호 : 807-88-00938 | 통신판매업신고 : 2019-서울강남-04455 호
						<span class="m_link">
							<a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
						</span>
					</div>
					<div class="foot_contact">
						<button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
							<i class="fal fa-paper-plane"></i>
						</button>
					</div>
				</div>
				<div class="col-xs-12 copyright">
					<p>Copyright &copy; Newturn Tree AD. All rights reserved.</p>
					<span>
						<a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
					</span>
				</div>
			</div><!-- row end -->
		</div><!-- Container_END -->
	</footer>

	<!-- Float-Call -->
	<div class="float-call">
		<a href="tel:02-544-1117" class="mobile_btn"><img src="<?=base_url('assets/images/icon/mobile-call.png')?>" alt="상담전화" title="상담전화"></a>
		<a href="/sub/contactus" class="desktop_btn"><img src="<?=base_url('assets/images/icon/desktop-call.png')?>" alt="상담문의" title="상담문의"></a>
	</div>
	<!-- Float-Call_END-->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

    <script src="<?=base_url('assets/js/common.js')?>"></script>
