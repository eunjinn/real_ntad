<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Newturn Tree AD">
    <meta name="description" content="The most effective way to reach customers, Rich Media">

    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="icon" type="image/x-icon">

    <!-- SNS -->
    <meta property="og:title" content="Newturn Tree AD" />
    <meta property="og:site_name" content="Newturn Tree AD"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://newturntreead.com/" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="Newturn Tree AD" />

    <title>Newturn Tree AD WEB</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?=base_url('assets/lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/lib/animate/animate.css')?>" rel="stylesheet" type="text/css">

    <link href="<?=base_url('assets/css/global.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/port.css')?>" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="<?=base_url('assets/images/logo.png')?>" />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/homepage">홈페이지</a>
                        </li>
                        <li>
                            <a href="/sub/shoppingmall">쇼핑몰</a>
                        </li>
                        <li>
                            <a href="/sub/mobile">모바일/앱</a>
                        </li>
                        <li>
                            <a href="/sub/landingpage">랜딩페이지</a>
                        </li>
                        <li>
                            <a href="/sub/tech">기술력</a>
                        </li>
                        <li>
                            <a href="/sub/portfolio">포트폴리오</a>
                        </li>
                        <li>
                            <a href="/sub/company">회사소개</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">견적문의</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Portfolio -->
    <section id="portfolio" class="wow fadeInUp mt60">
        <div class="container">
            <div class="section-header">
                <h3>Portfolio</h3>
                <p>
                    Newturn Tree AD의 역사는 고객님들과 함께한 성공적인 서비스를 토대로 쓰여지고 있습니다.
                </p>
            </div>
            <div class="row mt30">
                <div class="col-lg-12">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">전부</li>
                        <li data-filter=".filter-web">홈페이지</li>
                        <li data-filter=".filter-shop">쇼핑몰</li>
                        <li data-filter=".filter-mobile">모바일</li>
                    </ul>
                </div>
            </div>
            <div class="row portfolio-container">
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_1.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_1.gif')?>" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview">
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">남동인더스카이</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-mobile wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/mobile_1.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/mobile_1.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview">
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="#" class="link-details" title="More Details">
                                <i class="far fa-clone"></i>
                            </a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">메디컬 리포트</a></h4>
                            <p>모바일</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_2.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_2.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview">
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="#" class="link-details" title="More Details">
                                <i class="far fa-clone"></i>
                            </a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">서울시자원봉사센터</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-shop wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/shop_1.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/shop_1.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview">
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="#" class="link-details" title="More Details">
                                <i class="far fa-clone"></i>
                            </a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Yonafromspace</a></h4>
                            <p>쇼핑몰</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-mobile wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/mobile_2.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/mobile_2.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview">
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="#" class="link-details" title="More Details">
                                <i class="far fa-clone"></i>
                            </a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">리서치페이퍼</a></h4>
                            <p>모바일</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_3.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_3.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">V세상 플랫폼</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-shop wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/shop_2.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/shop_2.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">대명스포츠</a></h4>
                            <p>쇼핑몰</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-shop wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/shop_3.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/shop_3.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">크레용아이</a></h4>
                            <p>쇼핑몰</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-shop wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/shop_4.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/shop_4.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Mark 8ight</a></h4>
                            <p>쇼핑몰</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_4.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_4.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">HGInitiative</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_5.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_5.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">서울시여성가족재단<br>일가족양립센터</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_6.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_6.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">혜정문화재단</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_7.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_7.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">서울시베러월드스쿨</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_8.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_8.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Aiin AD</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_9.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_9.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">팸타임스</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_10.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_10.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">피에스타 스코프</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="<?=base_url('assets/images/portfolio/web_11.gif')?>" class="img-fluid" alt="">
                            <a href="<?=base_url('assets/images/portfolio/web_11.gif')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="far fa-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="far fa-clone"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">에이아이타임스</a></h4>
                            <p>홈페이지</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio_END -->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#top"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <? require_once(APPPATH."views/footer.php");?>
    <!-- Javascript files -->
    <script src="<?=base_url('assets/lib/cbpAnimatedHeader.js')?>"></script>
    <script src="<?=base_url('assets/lib/classie.js')?>"></script>
    <script src="<?=base_url('assets/lib/jquery.parallax-1.1.3.js')?>"></script>
    <script src="<?=base_url('assets/lib/sticky/jquery.sticky.js')?>"></script>
    <script src="<?=base_url('assets/lib/waypoints/waypoints.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/wow/wow.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/superfish/superfish.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/counterup/counterup.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/owlcarousel/owl.carousel.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/isotope/isotope.pkgd.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/lightbox/js/lightbox.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>

    <script src="<?=base_url('assets/js/port.js')?>"></script>
</body>
</html>
