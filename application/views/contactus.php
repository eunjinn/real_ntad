<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Newturn Tree AD">
    <meta name="description" content="The most effective way to reach customers, Rich Media">

    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="icon" type="image/x-icon">

    <!-- SNS -->
    <meta property="og:title" content="Newturn Tree AD" />
    <meta property="og:site_name" content="Newturn Tree AD"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://newturntreead.com/" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="Newturn Tree AD" />

    <title>Newturn Tree AD WEB</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?=base_url('assets/lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/lib/animate/animate.css')?>" rel="stylesheet" type="text/css">

    <link href="<?=base_url('assets/css/global.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/contactus.css')?>" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="<?=base_url('assets/images/logo.png')?>" />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/homepage">홈페이지</a>
                        </li>
                        <li>
                            <a href="/sub/shoppingmall">쇼핑몰</a>
                        </li>
                        <li>
                            <a href="/sub/mobile">모바일/앱</a>
                        </li>
                        <li>
                            <a href="/sub/landingpage">랜딩페이지</a>
                        </li>
                        <li>
                            <a href="/sub/tech">기술력</a>
                        </li>
                        <li>
                            <a href="/sub/portfolio">포트폴리오</a>
                        </li>
                        <li>
                            <a href="/sub/company">회사소개</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">견적문의</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Contact Us-->
    <section id="contactus" class="wow fadeInUp mt60">
        <div class="container">
            <div class="section-header">
                <h3>Contact Us</h3>
                <p>
                    Newturn Tree AD는 고객님의 이야기에 언제나 귀를 열고 있습니다.<br>
                    문의남겨주시면 신속히 연락드리겠습니다.
                </p>
            </div>
            <form class = "forrm">
                <h4 class="contact-title">개인정보처리방침</h4>
                <div class="scroll">
                    <div>
                        <b>1. 개인정보의 수집 및 이용 목적</b><br>
                        뉴턴트리AD(이하 회사)회사는 다음과 같은 목적을 위해 개인정보를 수집하고 이용합니다.<br>
                        <span>- 서비스 제공, 콘텐츠 제공, 웹 컨설팅 기초 자료 확보, 마케팅 활용, 상담 신청 처리, 구매 대행 및 요금 결제 등을 위한 원활한 의사소통 경로 확보</span><br><br>

                        <b>2. 수집하는 개인정보의 항목 및 수집방법</b><br>
                        회사는 다음과 같은 항목들을 수집하여 처리합니다.<br>
                        <span>- 필수항목 : 이름(업체명), 연락처, 이메일</span><br><br>

                        <b>3. 개인정보의 보유 및 이용기간</b><br>
                        회사는 원칙적으로 이용자의 개인정보 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br>
                        관계법령의 규정에 의하여 보존할 필요가 있는 경우에는 일정기간 동안 보존합니다.<br>
                        <span>- 문의하기 : 1년(정보통신망법)<br>
                        - 홈페이지 방문에 관한 기록 : 3개월(통신비밀보호법)<br>
                        - 소비자의 불만 또는 분쟁처리에 관한 기록 : 3년(전자상거래 등에서의 소비자 보호에 관한 법률)</span><br><br>

                        <b>4. 개인정보에 관한 민원서비스</b><br>
                        회사는 고객의 개인정보를 보호하고 개인정보와 관련한 불만을 처리하기 위하여 아래와 같이 관련 부서 및 개인정보관리책임자를 지정하고 있습니다.<br><br>

                        개인정보관리책임자 성명 : 황준범<br>
                        전화번호 : 02-544-1117<br>
                        이메일 : <a href="mailto:info@newturntreead.com">info@newturntreead.com</a><br><br>

                        회사의 서비스를 이용하시며 발생하는 모든 개인정보보호 관련 민원을 개인정보관리책임자 혹은 담당부서로 신고하실 수 있습니다.
                        회사는 이용자들의 신고사항에 대해 신속하게 충분한 답변을 드릴 것입니다.<br><br>

                        기타 개인정보침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다.<br><br>

                        1.개인정보침해신고센터 (www.1336.or.kr/ 국번없이 118)<br>
                        2.정보보호마크인증위원회 (www.eprivacy.or.kr/ 02-580-0533~4)<br>
                        3.대검찰청 인터넷범죄수사센터 (http://icic.sppo.go.kr/ 02-3480-3600)<br>
                        4.경찰청 사이버테러대응센터 (www.ctrc.go.kr/ 02-392-0330)
                    </div>
                </div>

                <div class="check_agree mt10">
                    <input type="checkbox" id="agree" name="agree" value="1" checked="checked">
                    <label for="agree" class="agree"> 개인정보처리방침에 동의해주세요.</label>
                </div>

                <h4 class="contact-title">기본정보<span>(* 항목은 필수입니다.)</span></h4>
                <div class="contact-box table-responsive">
                    <table class="table form">
                        <colgroup>
                            <col width="10">
                            <col width="40%">
                            <col width="10%">
                            <col width="40%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">제작구분</th>
                                <td colspan="3" style="padding: 7px 5px 9px 5px;">
                                    <input type="checkbox" class="consult_cat" value="홈페이지" name="type[]" style="vertical-align:middle;">
                                    <label for="cat_1"> 홈페이지</label>
                                    <input type="checkbox" class="consult_cat" value="쇼핑몰" name="type[]" style="vertical-align:middle;">
                                    <label for="cat_2"> 쇼핑몰</label>
                                    <input type="checkbox" class="consult_cat" value="모바일" name = "type[]" style="vertical-align:middle;">
                                    <label for="cat_3"> 모바일</label>
                                    <input type="checkbox" class="consult_cat" value="랜딩페이지" name = "type[]" style="vertical-align:middle;">
                                    <label for="cat_4"> 랜딩페이지</label>
                                    <input type="checkbox" class="consult_cat" value="유지보수" name = "type[]" style="vertical-align:middle;">
                                    <label for="cat_5"> 유지보수</label>
                                    <input type="checkbox" class="consult_cat" value="기타" name = "type[]" style="vertical-align:middle;">
                                    <label for="cat_6"> 기타</label>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="company">회사명</label>
                                </th>
                                <td>
                                    <input type="text" id="company" name="company">
                                </td>
                                <th scope="row">
                                    <span class="hide">(필수사항)</span>
                                    <label for="damdang">담당자*</label>
                                </th>
                                <td>
                                    <input type="text" class="w99 req" required = "required" id="damdang" name="manager">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <span class="hide">(필수사항)</span><label for="tell">연락처*</label>
                                </th>
                                <td>
                                    <input type="text" id="tell" name="tel" class = "req" required = "required">
                                </td>
                                <th scope="row">
                                    <span class="hide">(필수사항)</span>
                                    <label for="email1">이메일*</label>
                                </th>
                                <td>
                                    <input type="email" name="email" id="email1" class = "req" title="이메일 아이디 입력" required>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="cost">제작예산</label>
                                </th>
                                <td colspan="3">
                                    <input type="text" id="cost" name="money" title="제작예산" value=""> 만원
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h4 class="contact-title">방문신청</h4>
                <div class="contact-box table-responsive">
                    <table class="table form">
                        <colgroup>
                            <col width="25%">
                            <col width="*">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row"><label for="bang_chk">방문신청</label></th>
                                <td>
                                    <input type="checkbox" id="bang_chk" name="come">
                                    <label for="bang_chk">방문상담을 원하는 경우 체크해 주세요.</label>
                                </td>
                            </tr>
                            <!-- <tr id="bang_f1" style="display:none;">
                                <th scope="row"><label for="bang_date">방문일자</label></th>
                                <td>
                                    <input type="text" id="bang_date" name="bang_date" value="" placeholder="" class="hasDatepicker">
                                    <img src="/images/cal.gif" alt="달력" title="달력">
                                </td>
                            </tr>
                            <tr id="bang_f2" style="display:none;">
                                <th scope="row">
                                    <label for="bang_place">방문장소</label>
                                </th>
                                <td>
                                    <input type="text" id="bang_place" name="bang_place" value="" placeholder="" class="w99">
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>

                <h4 class="contact-title">추가정보<span>(선택사항)</span></h4>
                <div class="contact-box table-responsive">
                    <table class="table form">
                        <colgroup>
                            <col width="15%">
                            <col width="35%">
                            <col width="15%">
                            <col width="35%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <label for="now_site">현재사이트</label>
                                </th>
                                <td>
                                    <input type="text" id="now_site" name="homepage" value="" placeholder="http://" class="lg-input">
                                </td>
                                <th scope="row">
                                    <label for="re_page">예상페이지수</label>
                                </th>
                                <td>
                                    <input type="text" id="re_page" name="page" class="lg-input">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="ret_site1">참고사이트 1</label>
                                </th>
                                <td>
                                    <input type="text" id="ret_site1" name="site1" value="" placeholder="http://" class="lg-input">
                                </td>
                                <th scope="row">
                                    <label for="ret_site2">참고사이트 2</label>
                                </th>
                                <td>
                                    <input type="text" id="ret_site2" name="site2" value="" placeholder="http://" class="lg-input">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="attach1">파일첨부 1</label>
                                </th>
                                <td colspan="3">
                                    <input type="file" id="attach1" title="첨부파일 1" class="w99">
                                    <input type="hidden" id="file1" name="file1">
                                </td>
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="attach2">파일첨부 2</label>
                                </th>
                                <td colspan="3">
                                    <input type="file" id="attach2" title="첨부파일 2" class="w99">
                                    <input type="hidden" id="file2" name="file2">
                                </td>
                                <th></th>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- <div class="file" style="border:1px solid #dedede; border-top:0; padding:10px; margin-top:0;"><label for="attach1">파일첨부하기:</label> <input type="file" name="attach1" id="attach1" title="첨부파일 1" /> <input type="file" name="attach2" id="attach2" title="첨부파일 2" /></div> -->

                <h4 class="contact-title">기타 문의사항</h4>
                <textarea class="form contact-textarea" name="question" id="comment"></textarea>

                <div class="contact-button text-center mb20">
                    <button type="button" id="contactgo">문의메일 발송</button>
                    <button type="submit" style="display : none;"></button>
                </div>
            </form>
        </div>
    </section>

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#top"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->
    <? require_once(APPPATH."views/footer.php");?>
    <!-- Portfolio_END -->
    <script src="<?=base_url('assets/lib/sticky/jquery.sticky.js')?>"></script>
    <script src="<?=base_url('assets/lib/wow/wow.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/isotope/isotope.pkgd.min.js')?>"></script>
    <script src="<?=base_url('assets/js/mail.js')?>?<?=TESTING?>"></script>
    <script src="<?=base_url('assets/js/port.js')?>"></script>
</body>
</html>
