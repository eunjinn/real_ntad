<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	<meta name="format-detection" content="telephone=no">

	<meta name="keywords" content="Newturn Tree AD">
	<meta name="description" content="The most effective way to reach customers, Rich Media">

	<link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
	<link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="icon" type="image/x-icon">

	<!-- SNS -->
	<meta property="og:title" content="Newturn Tree AD" />
	<meta property="og:site_name" content="Newturn Tree AD"/>
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://newturntreead.com/" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="Newturn Tree AD" />

	<title>Newturn Tree AD WEB</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="<?=base_url('assets/lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/lib/venobox/venobox.css')?>" rel="stylesheet">
	<link href="<?=base_url('assets/lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('assets/lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('assets/lib/animate/animate.css')?>" rel="stylesheet" type="text/css">

	<link href="<?=base_url('assets/css/global.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/main.css')?>" rel="stylesheet" type="text/css">
</head>
<body>

	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/">
						<img src="<?=base_url('assets/images/logo.png')?>" title="로고" alt="로고"  />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/sub/homepage">홈페이지</a>
						</li>
						<li>
							<a href="/sub/shoppingmall">쇼핑몰</a>
						</li>
						<li>
							<a href="/sub/mobile">모바일/앱</a>
						</li>
						<li>
							<a href="/sub/landingpage">랜딩페이지</a>
						</li>
						<li>
							<a href="/sub/tech">기술력</a>
						</li>
						<li>
							<a href="/sub/portfolio">포트폴리오</a>
						</li>
						<li>
							<a href="/sub/company">회사소개</a>
						</li>
						<li>
							<a href="/sub/contactus">견적문의</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- Home -->
	<section id="home" class="screen-height">
		<div class="overlay"></div>
		<div class="bg_video">
			<video id="bgvid" src="<?=base_url('assets/video/main.mp4')?>" preload="auto" autoplay="1" loop="loop" muted="muted" poster="" http:="" www.newturntree.com="" images="" bg="" video_bg.jpg"="">
				Your browser does not support HTML5 video.
			</video>
		</div>
		<div class="intro">
			<div class="start mb20" style="font-size: 36px;">
				<img src="<?=base_url('assets/images/logo.png')?>" title="로고" alt="로고" />
			</div>
		</div>
		<a href="#business" title="스크롤 이동" alt="스크롤 이동">
			<div class="scroll-down">
				<span class="glyphicon glyphicon-menu-down"></span>
			</div>
		</a>
	</section>
	<!-- Home_END -->

	<!-- Business -->
	<section id="business">
		<div class="container mt30">
			<div class="row business-header wow fadeInUp">
				<div class="col-md-8 col-md-offset-2 col-sm-9 col-sm-offset-1 col-xs-12 mb30">
					<div class="row m0">
						<p>빠른 제작! 부담없는 가격! 확실한 퀄리티!</p>
						<h2>최신 트렌드 디자인의 고퀄리티 맞춤형 홈페이지 제작</h2>
					</div>
					<div class="row m0">
						<div class="col-xs-12">
							<img src="<?=base_url('assets/images/web.png')?>" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="row business-contents clearfix wow fadeInUp">
				<div class="col-sm-3">
					<a href="/sub/homepage">
						<div class="box-img">
							<img src="<?=base_url('assets/images/product/1.png')?>" title="홈페이지 페이지로 이동" alt="홈페이지 페이지로 이동">
							<p>Webpage</p>
							<p>홈페이지</p>
						</div>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="/sub/shoppingmall">
						<div class="box-img">
							<img src="<?=base_url('assets/images/product/2.png')?>" title="쇼핑몰 페이지로 이동" alt="쇼핑몰 페이지로 이동">
							<p>Shoppingmall</p>
							<p>쇼핑몰</p>
						</div>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="/sub/mobile">
						<div class="box-img">
							<img src="<?=base_url('assets/images/product/3.png')?>" title="모바일/앱 페이지로 이동" alt="모바일/앱 페이지로 이동">
							<p>Mobile & App</p>
							<p>모바일/앱</p>
						</div>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="/sub/tech">
						<div class="box-img">
							<img src="<?=base_url('assets/images/product/4.png')?>" title="랜딩페이지 페이지로 이동" alt="랜딩페이지 페이지로 이동">
							<p>Landingpage</p>
							<p>랜딩페이지</p>
						</div>
					</a>
				</div>
			</div>
		</div><!-- Container_END -->
	</section>
	<!-- Product_END -->

	<!-- Gallery -->
	<section id="gallery" class="wow fadeInUp">
		<div class="container">
			<div class="section-header">
				<h3>Portfolio</h3>
				<p>Newturn Tree AD의 포트폴리오입니다.</p>
			</div>
			<div class="owl-carousel gallery-carousel mt50">
				<a href="<?=base_url('assets/images/gallery/1.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/1.gif')?>" title="YonaFromSpace 홈페이지" alt="YonaFromSpace 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/2.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/2.gif')?>" title="크레용아이 쇼핑몰" alt="크레용아이 쇼핑몰">
				</a>
				<a href="<?=base_url('assets/images/gallery/3.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/3.gif')?>" title="대명스포츠 홈페이지" alt="대명스포츠 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/4.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/4.gif')?>" title="마크에이트 쇼핑몰" alt="마크에이트 쇼핑몰">
				</a>
				<a href="<?=base_url('assets/images/gallery/5.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/5.gif')?>" title="남동인더스카이 홈페이지" alt="남동인더스카이 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/6.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/6.gif')?>" title="서울시자원봉사센터 기관홈페이지" alt="서울시자원봉사센터 기관홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/7.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/7.gif')?>" title="V세상 플랫폼" alt="V세상 플랫폼">
				</a>
				<a href="<?=base_url('assets/images/gallery/8.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/8.gif')?>" title="V세상 블로그" alt="V세상 블로그">
				</a>
				<a href="<?=base_url('assets/images/gallery/9.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/9.gif')?>" title="HGInitiative 홈페이지" alt="HGInitiative 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/10.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/10.gif')?>" title="서울시여성가족재단 일가족양립센터 기관홈페이지" alt="서울시여성가족재단 일가족양립센터 기관홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/11.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/11.gif')?>" title="혜정문화재단 홈페이지" alt="혜정문화재단 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/12.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/12.gif')?>" title="서울시베러월드스쿨 홈페이지" alt="서울시베러월드스쿨 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/13.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/13.gif')?>" title="아인애드 홈페이지" alt="아인애드 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/14.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/14.gif')?>" title="팸타임스 홈페이지" alt="팸타임스 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/15.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/15.gif')?>" title="피에스타스코프 홈페이지" alt="피에스타스코프 홈페이지">
				</a>
				<a href="<?=base_url('assets/images/gallery/16.gif')?>" class="venobox" data-gall="gallery-carousel">
					<img src="<?=base_url('assets/images/gallery/16.gif')?>" title="에이아이타임스 홈페이지" alt="에이아이타임스 홈페이지">
				</a>
			</div>
		</div>

	</section>
	<!-- Gallery_END -->

	<!-- Clients -->
	<section id="clients" class="wow fadeInUp">
		<div class="container">
			<div class="section-header">
				<h3>Clients</h3>
				<p>Newturn Tree AD와 함께 해온 고객사들을 소개합니다.</p>
			</div>
			<div class="owl-carousel clients-carousel">
				<img src="<?=base_url('assets/images/clients/client-1.jpg')?>" title="SK innovation" alt="SK innovation">
				<img src="<?=base_url('assets/images/clients/client-2.jpg')?>" title="JW MARRIOTT" alt="JW MARRIOTT">
				<img src="<?=base_url('assets/images/clients/client-3.jpg')?>" title="HYUNDAI MOBIS" alt="HYUNDAI MOBIS">
				<img src="<?=base_url('assets/images/clients/client-4.jpg')?>" title="LG전자" alt="LG전자">
				<img src="<?=base_url('assets/images/clients/client-5.jpg')?>" title="SM Entertainment" alt="SM Entertainment">
				<img src="<?=base_url('assets/images/clients/client-6.jpg')?>" title="AMORE PACIFIC" alt="AMORE PACIFIC">
				<img src="<?=base_url('assets/images/clients/client-7.jpg')?>" title="대한치과교정학회" alt="대한치과교정학회">
				<img src="<?=base_url('assets/images/clients/client-8.jpg')?>" title="롯데백화점" alt="롯데백화점">
				<img src="<?=base_url('assets/images/clients/client-9.jpg')?>" title="T&I CULTURES" alt="T&I CULTURES">
				<img src="<?=base_url('assets/images/clients/client-10.jpg')?>" title="LENSSIS" alt="LENSSIS">
				<img src="<?=base_url('assets/images/clients/client-11.jpg')?>" title="TOOLnTOOL" alt="TOOLnTOOL">
				<img src="<?=base_url('assets/images/clients/client-12.jpg')?>" title="임규성한의원" alt="임규성한의원">
				<img src="<?=base_url('assets/images/clients/client-13.jpg')?>" title="HotelinHome" alt="HotelinHome">
				<img src="<?=base_url('assets/images/clients/client-14.jpg')?>" title="CLEF du bonheur" alt="CLEF du bonheur">
			</div>
		</div>
	</section>
	<!-- Clients_END -->

	<!-- Contactus -->
	<section id="contact" class="wow fadeInUp mb30">
		<div class="container">
			<div class="section-header">
				<h3>Contact Us</h3>
				<p>
					Newturn Tree AD는 고객님의 이야기에 언제나 귀를 열고 있습니다.<br>
					문의남겨주시면 신속히 연락드리겠습니다.
				</p>
			</div>
			<div class="row mt50">
				<div class="text-center">
					<button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">문의하기</button>
				</div>
			</div>
		</div>
	</section>
	<!-- Contactus_END -->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#top"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

	<? require_once(APPPATH."views/footer.php");?>
	<!-- Javascript files -->
	<script src="<?=base_url('assets/lib/cbpAnimatedHeader.js')?>"></script>
	<script src="<?=base_url('assets/lib/classie.js')?>"></script>
	<script src="<?=base_url('assets/lib/jquery.parallax-1.1.3.js')?>"></script>
	<script src="<?=base_url('assets/lib/sticky/jquery.sticky.js')?>"></script>
	<script src="<?=base_url('assets/lib/waypoints/waypoints.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/wow/wow.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/superfish/hoverIntent.js')?>"></script>
	<script src="<?=base_url('assets/lib/superfish/superfish.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/venobox/venobox.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/counterup/counterup.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/owlcarousel/owl.carousel.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/isotope/isotope.pkgd.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/lightbox/js/lightbox.min.js')?>"></script>
	<script src="<?=base_url('assets/lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>

	<script src="<?=base_url('assets/js/main.js')?>"></script>
</body>
</html>
