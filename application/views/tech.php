<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Newturn Tree AD">
    <meta name="description" content="The most effective way to reach customers, Rich Media">

    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="icon" type="image/x-icon">

    <!-- SNS -->
    <meta property="og:title" content="Newturn Tree AD" />
    <meta property="og:site_name" content="Newturn Tree AD"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://newturntreead.com/" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="Newturn Tree AD" />

    <title>Newturn Tree AD WEB</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?=base_url('assets/lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/lib/animate/animate.css" rel="stylesheet')?>" type="text/css">

    <link href="<?=base_url('assets/css/global.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/sub.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/tech.css')?>" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="<?=base_url('assets/images/logo.png')?>" />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/homepage">홈페이지</a>
                        </li>
                        <li>
                            <a href="/sub/shoppingmall">쇼핑몰</a>
                        </li>
                        <li>
                            <a href="/sub/mobile">모바일/앱</a>
                        </li>
                        <li>
                            <a href="/sub/landingpage">랜딩페이지</a>
                        </li>
                        <li>
                            <a href="/sub/tech">기술력</a>
                        </li>
                        <li>
                            <a href="/sub/portfolio">포트폴리오</a>
                        </li>
                        <li>
                            <a href="/sub/company">회사소개</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">견적문의</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Banner start -->
    <section id="sub-bg">
        <div class="contaier-fluid">
            <div class="subpage-banner">
                <div class="banner-cover">
                    <div class="container">
                        <div class="row wow fadeInLeft">
                            <p>풍부한 경험과 노하우로 다져진 기술력을 경험해보세요!</p>
                            <p>Technical Skills</p>
                            <p>홈페이지 제작은 홈페이지 전문가에게 맡겨야 좋은 홈페이지를 만들 수 있습니다.<br>Newturn Tree AD는 풍부한 경험과 뛰어난 기술력을 바탕으로 작업을 합니다.</p>
                        </div>
                    </div>
                </div>
                <div class="banner-background tech"></div>
                <div class="container banner-img">
                    <div class="tech">
						<img class="wow fadeInDown" src="<?=base_url('assets/images/tech/tech.png')?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner_END -->

    <!-- Standard -->
    <section id="standard" class="sub-position wow fadeInUp">
        <div class="container">
            <div class="section-header mt120">
                <h3>웹 표준이란?</h3>
                <p>
                    웹 표준을 개발하는 국제 컨소시엄 W3C(World Wide Web Consortium)가 <br>
                    권고한 웹 표준안에 따라 웹페이지를 제작하여 사용자가 어떠한 브라우저로 웹페이지에 접속하더라도 <br>
                    동일한 결과를 볼 수 있도록 하는 것을 의미합니다.<br><br>
                    뉴턴트리 AD WEB은 계속해서 Validation과 Crossbrowsing 체크를 통해<br>
                    W3C가 권고하는 Markup & CSS 방식으로 웹페이지를 제작합니다.
                </p>
                <img class="w3c-mark" src="<?=base_url('assets/images/tech/w3c.png')?>" alt="">
            </div>
        </div>
    </section>
    <!-- Standard_END -->

    <!-- Accessibility -->
    <section id="accessibilitye" class="wow fadeInUp">
        <div class="container">
            <div class="section-header mt80">
                <h3>웹 접근성이란?</h3>
                <p>
                    웹 사이트 이용에 있어 장애인과 비장애인이든 상관없이 어떠한 상황이나 환경에 구애받지 않도록<br>
                    누구나 접근이 용이하도록 환경을 구축하는 것을 의미합니다.<br><br>
                    뉴턴트리 AD WEB은 NIA 한국정보화진흥원에서 제공하는<br>
                    <b>'한국형 웹 콘텐츠 접근성 지침 2.0’</b>에 따라 웹페이지를 제작합니다.
                </p>
                <img class="nia-mark" src="<?=base_url('assets/images/tech/nia.png')?>" alt="">
                <img class="access-mark" src="<?=base_url('assets/images/tech/access.png')?>" alt="">
            </div>
            <div class="row access">
                <div class="col-xs-12 access-title">
                    <p>&lt; 한국형 웹 콘텐츠 접근성 지침 2.0 &gt;</p>
                    <p>4가지 준수사항</p>
                </div>
                <div class="col-md-3 col-xs-12">
                    <img src="<?=base_url('assets/images/tech/1.png')?>" alt="">
                    <p class="title">인식의 용이성</p>
                </div>
                <div class="col-md-9 col-xs-12">
                    <p>모든 콘텐츠는 사용자가 인식할 수 있어야 한다.</p>
                    <p>
                        <b>대체 텍스트</b> 텍스트 아닌 콘텐츠에는 대체 텍스트를 제공해야 한다.<br>
                        <b>멀티미디어 대체 수단</b> 동영상, 음성등 멀티미디어 콘텐츠를 이해할 수 있도록 대체 수단을 제공해야 한다.<br>
                        <b>명료성</b> 콘텐츠는 명확하게 전달되어야 한다.
                    </p>
                </div>
                <div class="col-md-3 col-xs-12">
                    <img src="<?=base_url('assets/images/tech/2.png')?>" alt="">
                    <p class="title">운용의 용이성</p>
                </div>
                <div class="col-md-9 col-xs-12">
                    <p>사용자 인터페이스 구성요소는 조작 가능하고 내비게이션 할 수 있어야 한다.</p>
                    <p>
                        <b>키보드 접근성</b> 콘텐츠는 키보드로 접근할 수 있어야 한다.<br>
                        <b>충분한 시간 제공</b> 콘텐츠를 읽고 사용하는 데 충분한 시간을 제공해야 한다.<br>
                        <b>광과민성 발작 예방</b> 광과민성발작을 일으킬 수 있는 콘텐츠를 제공하지 않아야 한다.<br>
                        <b>쉬운 내비게이션</b> 콘텐츠는 쉽게 내비게이션 할 수 있어야 한다.
                    </p>
                </div>
                <div class="col-md-3 col-xs-12">
                    <img src="<?=base_url('assets/images/tech/3.png')?>" alt="">
                    <p class="title">이해의 용이성</p>
                </div>
                <div class="col-md-9 col-xs-12">
                    <p>콘텐츠는 이해할 수 있어야 한다.</p>
                    <p>
                        <b>가독성</b> 콘텐츠는 읽고 이해하기 쉬워야 한다.<br>
                        <b>예측 가능성</b> 콘텐츠의 기능과 실행결과는 예측 가능해야 한다.<br>
                        <b>콘텐츠의 논리성</b> 콘텐츠는 논리적으로 구성해야 한다.<br>
                        <b>입력 도움</b> 입력 오류를 방지하거나 정정할 수 있어야 한다.
                    </p>
                </div>
                <div class="col-md-3 col-xs-12">
                    <img src="<?=base_url('assets/images/tech/4.png')?>" alt="">
                    <p class="title">견고성</p>
                </div>
                <div class="col-md-9 col-xs-12">
                    <p>웹콘텐츠는 미래의 기술로도 접근할 수 있도록 견고하게 만들어야 한다.</p>
                    <p>
                        <b>문법 준수</b>웹 콘텐츠는 마크업 언어의 문법을 준수해야 한다.<br>
                        <b>웹 애플리케이션 접근성</b> 웹 애플리케이션은 접근성이 있어야 한다
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- Accessibility_END -->

    <!-- Respon & Adaptive -->
    <section id="respon_adapt" class="wow fadeInUp mb50">
        <div class="container">
            <div class="section-header">
                <h3>반응형 웹 vs 적응형 웹</h3>
                <p>반응형과 적응형이 어떤점이 다른지에 대한 비교분석을 통해<br>사용자에게 가장 적합한 홈페이지 제작을 제안해드립니다.</p>
            </div>
            <div class="row respon_adapt mt30">
                <div class="col-md-6">
                    <div class="text-center">
                        <img src="<?=base_url('assets/images/tech/respon.png')?>" alt="">
                    </div>
                    <div class="title">
                        <div class="col-xs-12">
                            <p>반응형 웹 (RWD : Responsive Web Design)</p>
                            <p>모든 디바이스 환경에 맞는 최적의 화면을 출력.</p>
                        </div>
                    </div>
                    <div class="contents">
                        <div class="col-md-6">
                            <p>장점</p>
                            <p>홈페이지 하나로 모든 디바이스 환경에 대응이 가능하다.</p>
                            <p>디바이스 환경에 따른 사이즈 변화가 자연스럽다.</p>
                            <p>단일 URL이라 검색노출에 유리.</p>
                        </div>
                        <div class="col-md-6">
                            <p>단점</p>
                            <p>적응형에 비해 이미지나 텍스트 가독성이 낮고 로딩속도가 저하된다.</p>
                            <p>레이아웃을 제한적으로 사용되어 단조로운 UI가 되어버린다.</p>
                            <p>컨텐츠 내용이 많을 경우 반응형에 부적합하다.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-center">
                        <img src="<?=base_url('assets/images/tech/adaptive.png')?>" alt="">
                    </div>
                    <div class="title">
                        <div class="col-xs-12">
                            <p>적응형 웹 (AWD : Adaptive Web Design)</p>
                            <p>사전에 정해놓은 디바이스별 환경에 따라 최적의 화면을 출력.</p>
                        </div>
                    </div>
                    <div class="contents">
                        <div class="col-md-6">
                            <p>장점</p>
                            <p>반응형에 비해 이미지나 텍스트 가독성이 높고 로딩속도가 상승된다.</p>
                            <p>레이아웃 사용이 자유로워 디바이스별 최적의 UI/UX를 구성할 수 있다.</p>
                            <p>디바이스를 개별적으로 관리하기 용이하다.</p>
                        </div>
                        <div class="col-md-6">
                            <p>단점</p>
                            <p>모든 디바이스 사이즈에 대한 대응이 불가하다.</p>
                            <p>디바이스 사이즈가 단계적으로 변화하여 다소 부자연스럽다.</p>
                            <p>디바이스별 제작비용이 발생한다.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Respon & Adaptive_END -->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#top"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->
    <? require_once(APPPATH."views/footer.php");?>
    <!-- Javascript files -->
    <script src="<?=base_url('assets/lib/sticky/jquery.sticky.js')?>"></script>
    <script src="<?=base_url('assets/lib/wow/wow.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/isotope/isotope.pkgd.min.js')?>"></script>

    <script src="<?=base_url('assets/js/port.js')?>"></script>
