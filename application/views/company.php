<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Newturn Tree AD">
    <meta name="description" content="The most effective way to reach customers, Rich Media">

    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?=base_url('assets/images/favicon/favicon.png')?>" rel="icon" type="image/x-icon">

    <!-- SNS -->
    <meta property="og:title" content="Newturn Tree AD" />
    <meta property="og:site_name" content="Newturn Tree AD"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://newturntreead.com/" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="Newturn Tree AD" />

    <title>Newturn Tree AD WEB</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=b0950cb6de2d2c765bdfb6778343c2e8"></script>
    <link href="<?=base_url('assets/lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/lib/animate/animate.css')?>" rel="stylesheet" type="text/css">

    <link href="<?=base_url('assets/css/global.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/company.css')?>" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="<?=base_url('assets/images/logo.png')?>" />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/homepage">홈페이지</a>
                        </li>
                        <li>
                            <a href="/sub/shoppingmall">쇼핑몰</a>
                        </li>
                        <li>
                            <a href="/sub/mobile">모바일/앱</a>
                        </li>
                        <li>
                            <a href="/sub/landingpage">랜딩페이지</a>
                        </li>
                        <li>
                            <a href="/sub/tech">기술력</a>
                        </li>
                        <li>
                            <a href="/sub/portfolio">포트폴리오</a>
                        </li>
                        <li>
                            <a href="/sub/company">회사소개</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">견적문의</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Portfolio -->
    <section id="company" class="wow fadeInUp mt60">
        <div class="container">
            <div class="company-header">
                <h3>About</h3>
                <img src="<?=base_url('assets/images/company/slogan_2.png')?>" alt="">
                <p>
                    Sir Isaac Newton은 현대과학의 문을 연 위대한 과학자입니다.<br>
                    뉴턴은 떨어지는 사과에서 만유인력의 법칙을 깨달았고,이는 인류 과학사의 새로운 전환점이 되었습니다.<br>
                    다른 사람들은 대수롭지 않게 지나갈 일이었으나 그는 현상에서 본질을 파악하여 새로운 가치를 찾아냈습니다.<br>
                    그 위대한 업적은 한 그루의 사과나무에서 시작되었습니다.<br><br>
                    마케팅 메세지의 포화 속에서 우리는 아직도 많은 사과들을 놓치며 살아가고 있습니다.<br>
                    뉴턴트리AD는 당신의 경쟁자들이 대수롭지 않게 지나가는 현상 속에 본질을 파악하여,<br>
                    당신의 새로운 전환점이 되겠습니다
                </p>
                <p>
                    그 시작은 이곳, <b>뉴턴트리 AD</b>입니다.
                </p>
            </div>
            <hr class="mt50">
            <div class="services-area area-padding">
                <div class="container">
                    <div class="row text-center">
                        <div class="services-contents">
                            <!-- Start Left services -->
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="about-move wow fadeInRight">
                                    <div class="services-details">
                                        <div class="single-services">
                                            <span class="services-icon" href="#">
                                                <i class="fad fa-bullseye-arrow"></i>
                                            </span>
                                            <h4>Responsibility</h4>
                                            <p>
                                                뉴턴트리AD는 언제나 클라이언트를 자사처럼 생각하며,<br>
                                                클라이언트의 목표를 달성하겠다는<br>
                                                책임감을 가지고 있습니다.
                                            </p>
                                        </div>
                                    </div>
                                    <!-- end about-details -->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="about-move wow fadeInUp">
                                    <div class="services-details">
                                        <div class="single-services">
                                            <span class="services-icon" href="#">
                                                <i class="fad fa-analytics"></i>
                                            </span>
                                            <h4>Analytical thinking</h4>
                                            <p>
                                                뉴턴트리AD는 기획자의 주관적인 근거가 아닌<br>
                                                정확한 시장 데이터 분석을 근거로 클라이언트가<br>
                                                직면한 문제를 해결하는 사고를 하고 있습니다.
                                            </p>
                                        </div>
                                    </div>
                                    <!-- end about-details -->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <!-- end col-md-4 -->
                                <div class="about-move wow fadeInLeft">
                                    <div class="services-details">
                                        <div class="single-services">
                                            <span class="services-icon" href="#">
                                                <i class="fad fa-user-chart"></i>
                                            </span>
                                            <h4>Problem solving ability</h4>
                                            <p>
                                                뉴턴트리AD는 고객이 문제에 직면했을 때<br>
                                                문제해결의 결과물을 모든 면에서 기존의 방식이 아닌<br>
                                                최초, 최고, 차별화를 지향하는 마음가짐을 가집니다.
                                            </p>
                                        </div>
                                    </div>
                                    <!-- end about-details -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio_END -->

    <!-- Clients -->
    <section id="visit" class="wow fadeInUp">
        <div class="container">
            <div class="section-header mb50">
                <h3>Visit Office</h3>
                <p>방문상담을 원하실 경우 반드시 사전에 전화로 스케쥴을 잡아주시면 감사하겠습니다</p>
            </div>
            <div class="row">
                <div id="map" style=" width:100%; height:400px;"></div>
            </div>
        </div>
    </section>
    <!-- Clients_END -->
    <? require_once(APPPATH."views/footer.php");?>
    <!-- Javascript files -->
    <script src="<?=base_url('assets/lib/sticky/jquery.sticky.js')?>"></script>
    <script src="<?=base_url('assets/lib/wow/wow.min.js')?>"></script>
    <script src="<?=base_url('assets/lib/isotope/isotope.pkgd.min.js')?>"></script>

    <script src="<?=base_url('assets/js/companymap.js')?>"></script>
    <script src="<?=base_url('assets/js/port.js')?>"></script>
</body>
</html>
