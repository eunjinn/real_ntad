(function($){

	/* Preloader */
	$(window).load(function() {
		$('#status').fadeOut();
		$('#preloader').delay(300).fadeOut('slow');
	});

	$(document).ready(function() {

		/* Clients carousel (uses the Owl Carousel library) */		 
		$(".clients-carousel").owlCarousel({
			autoplay: true,
			dots: true,
			loop: true,
			responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }	}
		});

		/* Initialize Venobox */
		$('.venobox').venobox({
			bgcolor: '',
			overlayColor: 'rgba(6, 12, 34, 0.85)',
			closeBackground: '',
			closeColor: '#fff'
		});

		/* Gallery carousel (uses the Owl Carousel library) */
		$(".gallery-carousel").owlCarousel({
			autoplay: true,
			dots: true,
			loop: true,
			center:true,
			responsive: { 0: { items: 1 }, 768: { items: 3 }, 992: { items: 4 }, 1200: {items: 5 } }
		});

		/* Smooth scroll / Scroll To Top */
		$('a[href*=#]').bind("click", function(e){
			var anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $(anchor.attr('href')).offset().top
			}, 1000);
			e.preventDefault();
		});

		$(window).scroll(function() {
			if ($(this).scrollTop() > 100) {
				$('.scroll-up').fadeIn();
			} else {
				$('.scroll-up').fadeOut();
			}
		});

		/* Navbar */
		$('.header').sticky({
			topSpacing: 0
		});

		$('body').scrollspy({
			target: '.navbar-custom',
			offset: 70
		})

		/* Home BG */
		$(".screen-height").height($(window).height());

		$(window).resize(function(){
			$(".screen-height").height($(window).height());
		});

		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
			$('#home').css({'background-attachment': 'scroll'});
		} else {
			$('#home').parallax('50%', 0.1);
		}

		/* WOW Animation When You Scroll */
		wow = new WOW({
			mobile: false
		});
		wow.init();

	});

})(jQuery);